document.addEventListener("DOMContentLoaded", function() {

	// Custom JS

});

jQuery( document ).ready(function($) {

  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

	$( '.nav__item' ).on('click',function() {
  	  if ( !$( this ).parent().is( '.nav__item-current') ) {
  	  	$( '.nav__item' ).removeClass('nav__item-current');
  	  	$( this ).addClass('nav__item-current');
  	  } 
    });

    $( '.menu-mobile' ).on('click',function() {
      	$('.header__nav').toggle();
    });

		
	$('.batteries-slider').slick({
		autoplay: true, 
		slidesToScroll: 1,
		slidesToShow: 1,
		dots: false,
	    prevArrow: false,
	    nextArrow: false
	});

	$('.smartphone-slider').slick({
		autoplay: true, 
		slidesToScroll: 1,
		slidesToShow: 1,
		dots: false,
	    prevArrow: false,
	    nextArrow: false
	});
	
	
	

});
